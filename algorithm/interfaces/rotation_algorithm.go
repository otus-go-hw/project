package interfaces

import "gitlab.com/otus-go-hw/project/algorithm/entities"

type RotationAlgorithm interface {
	GetHandle([]entities.AlgorithmData) (int64, error)
}
